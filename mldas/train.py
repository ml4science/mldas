#!/usr/bin/env python

"""
Main training script for DAS machine learning
"""

__copyright__ = """
Machine Learning for Distributed Acoustic Sensing data (MLDAS)
Copyright (c) 2020, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of
any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.
"""
__license__ = "Modified BSD license (see LICENSE.txt)"
__maintainer__ = "Vincent Dumont"
__email__ = "vincentdumont11@gmail.com"

# System
import os
import logging

# Externals
import yaml
import numpy
import torch
import torchinfo
import torch.distributed as dist
from torchvision import models

# Locals
from .datasets import get_data_loaders
from .trainers import get_trainer
from .utils.logging import config_logging
from .utils.distributed import init_workers

def load_config(config_file=None,**kwargs):
    """
    """
    config = {}
    if config_file!=None:
        with open(config_file) as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
    config.update({k:v for (k,v) in kwargs.items() if k not in config.keys()})
    update = {k:v for (k,v) in kwargs.items() if v}
    config['output_dir'] = os.path.expandvars(config['output_dir'])
    os.makedirs(config['output_dir'], exist_ok=True)
    return config

def main(rank=0,size=1,device=torch.device('cpu'),device_ids=[],backend=None,trainer_out=False,**kwargs):
    """
    Main function to perform deep learning training on DAS data.
    """
    # Load the datasets
    is_distributed = backend is not None
    train_data_loader, valid_data_loader, test_data_loader = get_data_loaders(is_distributed, **kwargs)
    target_data, label_data = next(iter(train_data_loader))
    input_size = target_data.reshape(target_data.shape[0],-1).shape[1]
    output_size = label_data.reshape(label_data.shape[0],-1).shape[1]
    trainer = get_trainer(rank=rank, device=device, **kwargs)
    # Build the model
    batch_input, batch_target = next(iter(train_data_loader))
    trainer.build_model(device_ids,distributed=is_distributed,**kwargs)
    if trainer_out:
        return trainer
    logging.info('-'*40)
    logging.info('Number of parameters : {:,d}'.format(sum([p.nelement() for p in trainer.model.parameters()])))
    # logging.info(str(torchinfo.summary(trainer.model,input_size=batch_input.shape,verbose=False)).split('\n')[-4])
    logging.info('-'*40)
    # Run the training
    summary = trainer.train(train_data_loader=train_data_loader, valid_data_loader=valid_data_loader, test_data_loader=test_data_loader, **kwargs)
    trainer.write_summaries()
    return summary['test_loss'][0]

def train(config=None,distributed_backend=None,**kwargs):
    # Load configuration
    config = load_config(config,**kwargs)
    # Initialization
    rank, size, device, device_ids = init_workers(distributed_backend)
    # Setup logging
    log_file = os.path.join(config['output_dir'], 'out_%02i.log' % (rank+1))
    config_logging(log_file=log_file, **kwargs)
    logging.info('Initialized rank %i out of %i'%(rank,size))
    run_training(rank, size, device, device_ids, distributed_backend, **config)
    